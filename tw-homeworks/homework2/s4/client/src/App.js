import React, { Component } from 'react';
import './App.css';
import {ProductList} from './components/ProductList';

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.productList = [];
  }
  
  async componentDidMount(){
    let productList = await this.getAllProducts();
    this.setState({
      productList: productList
    })
  }
  
  getAllProducts = async () => {
    let response = await fetch('http://localhost:8090/get-all');
    let products = await response.json();
    return products;
  }
  
  render() {
    return (
      <div className="container">
        <ProductList title="Product List" source={this.state.productList}/>
      </div>
    );
  }
}

export default App;
