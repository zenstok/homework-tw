import React from 'react';
import AddProduct from './AddProduct'

export class ProductList extends React.Component {
    
    transformItems = () => {
                return this.props.source.map((element, index) => {
                    return <div key={index}>
                    <h4>- {element.productName} - Price: {element.price}</h4>
                    </div>
                })
         
    }
    
    render(){
        let items = this.transformItems();

        return (
            <div>
            <h1>{this.props.title}</h1>
                {items}
            <AddProduct onAdd={this.add}/>
            </div>
            );
    }   
}