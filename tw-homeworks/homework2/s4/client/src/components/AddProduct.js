import React, { Component } from 'react';

class AddProduct extends Component {
    constructor(props){
        super(props)
        this.state = {
            productName : '',
            price : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            }) 
        }
    }


    submit() {
        let data={}
        data.productName = this.state.productName
        data.price = this.state.price

        fetch("http://localhost:8090/add", {
            method: 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })

        }).catch(error => console.log('Error:', error))
            .then(response => console.log('Success:', response));
    }
  render() {
    return (
      <div>
        <form>
            <label htmlFor="productName">Product Name</label>
            <input type="text" name="productName" id="productName" onChange={this.handleChange} />
            <label htmlFor="price">Price</label>
            <input type="text" name="price" id="price" onChange={this.handleChange} />
            <input type="button" value="add" onClick={() => this.submit()} />

        </form>
      </div>
    )
  }
}

export default AddProduct
