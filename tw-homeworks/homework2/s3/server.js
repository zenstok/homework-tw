const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update/:id', function (req, res) {
    let product = products.find(product => product.id == req.params.id);
    if(typeof product !== 'undefined') {
        if(req.body.productName && req.body.price){
            product.productName = req.body.productName;
            product.price = req.body.price;
            res.status(500).send('Product updated!');
        } else {
            res.status(500).send("Product found, but didn't find all body parameters required!");
        }    
    }
    else
        res.status(500).send('Cannot find product with given ID!');

});

app.delete('/delete', function (req, res) {
    if(typeof req.body.productName !== 'undefined') {
        let productIndex = products.findIndex(product => product.productName == req.body.productName);
        if(productIndex !== -1) {
            products.splice(productIndex,1);
            res.status(500).send('Product deleted!');
        }
        else
            res.status(500).send('Cannot find product with given name!');
    }
    else 
       res.status(500).send('Body parameter "productName" is missing!"'); 

});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});